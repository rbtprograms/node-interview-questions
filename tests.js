/*
 * This file was set up to help the tests run in the proper order. I was having
 * problems with delete running before get and update, so I broke out the
 * ordering into a new file to make sure the tests would all run the way I
 * wanted them to 
*/

require('./test/adduser.js');
require('./test/getusers.js');
require('./test/updateuser.js');
require('./test/deleteuser.js');