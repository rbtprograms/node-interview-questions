var express = require('express');
var router = express.Router();

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.find({},{},function(err, docs){
        if(err) res.send('An error occurred. Error: ' + err);
        if(!docs) res.status(204).send({ msg: 'No documents found' });
        return res.json(docs);
    });
});

/*
 * POST to adduser.
 */
router.post('/adduser', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    if(Object.keys(req.body).length === 0) {
        res.status(404)
            .send({ msg: 'No data included in request. Please check your request body'});
        return;
    }
    if(!req.body.username || !req.body.email || !req.body.fullname || !req.body.age || !req.body.location || !req.body.gender) {
        res.status(404)
            .send({ msg: 'Incomplete data received. Please check your request body to make sure all fields are complete'});
        return;
    }
    else {
        collection.insert(req.body, function(err, result){
            if (err) {
                res.status(500).send({ msg: 'An error occurred. Error: ' + err});
                return;
            };
            return res.status(201).json(result)
        });
    }
});

/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToDelete = req.params.id;
    collection.remove({ '_id' : userToDelete }, function(err, result) {
        const { n } = result.result;
        if(!n) {
            res.status(500).send({ msg: 'Error deleting document'});
            return;
        }
        return res.send({ removed: true });
    });
});

/*
 * UPDATE to updateusers.
 */
router.put('/updateuser/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToUpdate = req.params.id;
    if(Object.keys(req.body).length === 0) {
        res.status(404)
            .send({ msg: 'No data included in request. Please check your request body'});
        return;
    }
    collection.findOneAndUpdate(
        userToUpdate,
        req.body,
    )
    .then((user) => {
        if(!user) {
            res.status(404).send({ msg: 'No entry found. Please check your request body to make sure your data is correct'})
            return
        }
        return res.json(user);
    })
});

module.exports = router;