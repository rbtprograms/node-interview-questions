import { get, post, del } from './request'

const BASE_URL = 'http://localhost:3000/users';

export function getUsers() {
  return get(`${BASE_URL}/userlist`);
};

export function addUser(user) {
  const url = `${BASE_URL}/adduser`
  return post(url, user);
};

export function deleteUser(id) {
  const url = `${BASE_URL}/deleteuser/${id}`
  return del(url, id);
}