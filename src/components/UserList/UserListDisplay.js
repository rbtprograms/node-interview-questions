import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';


class UserListDisplay extends Component {

  static propTypes = {
    user: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired
  }

  render() { 
    const { user, onDelete, onSelect } = this.props;

    return (
      <Fragment>
        <td><a onClick={onSelect}>{user.username}</a></td>
        <td>{user.email}</td>
        <td><a onClick={onDelete}>delete</a></td>
      </Fragment>
    );
  }
}
 
export default UserListDisplay;