import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import UserListDisplay from './UserListDisplay';

class User extends PureComponent {

  static propTypes = {
    user: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
  }

  handleDelete = () => {
    const { user, onRemove } = this.props;
    return onRemove(user._id);
  }

  handleSelect = () => {
    const { user, onSelect } = this.props;
    return onSelect(user);
  }

  render() { 
    const { user, onSelect } = this.props;

    return (
      <tr>
        <UserListDisplay 
          user={user} 
          onDelete={this.handleDelete}
          onSelect={this.handleSelect} 
        />
      </tr>
    );
  }
}
 
export default User;