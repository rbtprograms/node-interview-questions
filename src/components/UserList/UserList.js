import React, { Component } from 'react';
import PropTypes from 'prop-types';
import User from './User';
import styles from './UserList.css';

class UserDisplay extends Component {

  static propTypes = {
    users: PropTypes.array,
    onSelect: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
  }

  render() { 
    const { users, onSelect, onRemove } = this.props;

    return (
      <table className={styles.user_list}>
        <thead>
          <tr>
            <th>UserName</th>
            <th>Email</th>
            <th>Delete?</th>
          </tr>
          {users && users.map(user => (
            <User 
              key={user._id} 
              user={user} 
              onSelect={onSelect}
              onRemove={onRemove}
              />
          ))}
        </thead>
      </table>
    );
  }
}
 
export default UserDisplay;