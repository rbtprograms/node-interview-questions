import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './UserAdd.css';


class UserAdd extends Component {

  state = {
    username: '',
    email: '',
    fullname: '',
    age: '',
    location: '',
    gender: ''
  }

  static propTypes = {
    onComplete: PropTypes.func.isRequired
  }

  handleChange = ({ target }) => {
    this.setState({ [target.placeholder]: target.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    this.props.onComplete(this.state);
    this.setState({ username: '', email: '', fullname: '', age: '', location: '', gender: ''})
  }

  render() { 
    const { username, email, fullname, age, location, gender } = this.state;

    return (
      <form onSubmit={this.handleSubmit} className={styles.add}>
        <h3>Add User</h3>
        <InputControl placeholder='username' value={username} onChange={this.handleChange}/>
        <InputControl placeholder='email' value={email} onChange={this.handleChange}/>
        <InputControl placeholder='fullname' value={fullname} onChange={this.handleChange}/>
        <InputControl placeholder='age' value={age} onChange={this.handleChange}/>
        <InputControl placeholder='location' value={location} onChange={this.handleChange}/>
        <InputControl placeholder='gender' value={gender} onChange={this.handleChange}/>
        <button type='submit'>Add</button>
      </form>
    );
  }
}

const InputControl = (props) => (
  <p>
    <label>
      <input {...props} required/>
    </label>
  </p>
);
 
export default UserAdd;