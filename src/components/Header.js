import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Header.css';

class Header extends Component {
  render() { 
    return (
      <section className={styles.header}>
        <h1>SBI Software Code Challenge</h1>
        <h3>Robert Thompson's Submission</h3>
      </section>
    );
  }
}
 
export default Header;