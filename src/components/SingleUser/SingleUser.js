import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './SingleUser.css';

class SingleUser extends Component {

  static propTypes = {
    user: PropTypes.object
  }

  render() { 
    const { user } = this.props;
    return (
      <ul className={styles.single_user}>
        <li>Full Name: {user ? user.fullname : ''}</li>
        <li>Gender: {user ? user.gender : ''}</li>
        <li>Age: {user ? user.age : ''}</li>
        <li>Location: {user ? user.location : ''}</li>
      </ul>
    );
  }
}
 
export default SingleUser;