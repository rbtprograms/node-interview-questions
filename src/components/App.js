import React, { Component } from 'react';
import Header from './Header';
import UserList from './UserList/UserList';
import UserAdd from './UserAdd/UserAdd';
import SingleUser from './SingleUser/SingleUser';
import { getUsers, addUser, deleteUser } from '../services/usersApi';
import styles from './App.css';

class App extends Component {

  state = {
    users: null,
    selectedUser: null
  }

  componentDidMount() {
    getUsers()
      .then(_users => {
        this.setState({ users: _users });
      });
  }

  handleAdd = user => {
    return addUser(user)
      .then(added => {
        this.setState(({ users }) => {
          return {
            users: [...users, added]
          };
        });
      });
  };

  handleRemove = id => {
    return deleteUser(id)
      .then(() => {
        this.setState(({ users }) => {
          return {
            users: users.filter(user => user._id !== id)
          };
        });
      });
  };

  handleSelectedUser = user => {
    this.setState({ selectedUser: user });
  }
  
  render() { 
    const { users, selectedUser } = this.state;

    return (
      <div>
        <Header/>
        <div className={styles.app}>
          <SingleUser user={selectedUser}/>
          <UserList 
            users={users} 
            onSelect={this.handleSelectedUser}
            onRemove={this.handleRemove}
          />
          <UserAdd onComplete={this.handleAdd}/>
        </div>
      </div>
    );
  }
};
 
export default App;