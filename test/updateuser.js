var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');

var url = 'http://localhost:3000';

describe('Update User', function () {
	it('updates the \'test user\' from the database', function (done) {
		var updatedUser = {
			'username' : 'test user',
			'email' : 'test3@test.com', //new email!
			'fullname' : 'Bob Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};
			
		var db = monk('localhost:27017/node_interview_question');
		var collection = db.get('userlist');
		collection.findOne({ 'username': 'test user' }, function (err, doc) {
			var userId = doc._id;
			request(url)
				.put('/users/updateuser/' + userId)
				.send(updatedUser)
				.expect(200)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(res));
						throw err;
					}
					expect(res.body).toBeTruthy();
					expect(res.body.username).toBe('test user')
					assert.equal(res.body.email, updatedUser.email);
					db.close();
					done();
				});
		});
	});

	it('should return a 404 when no document was found', function (done) {			
		var db = monk('localhost:27017/node_interview_question');
		var collection = db.get('userlist');
		request(url)
			.put('/users/updateuser/' + '5bb3eab6970b0217d0d2bf75')
			.send({
				'username' : 'hi',
				'email' : 'none',
				'fullname' : 'nope',
				'age' : 27,
				'location' : 'San Francisco',
				'gender' : 'Male'
			})
			.expect(404)
			.end(function (err, res) {
				if (err) {
					console.log(JSON.stringify(res));
					throw err;
				}
				expect(res.body).toBeTruthy();
				expect(res.body.username).toBeUndefined();
				assert.deepEqual(res.body, { msg: 'No entry found. Please check your request body to make sure your data is correct' });
				db.close();
				done();
			});
	});

	it('should return a 404 when no request body was included', function (done) {			
		var db = monk('localhost:27017/node_interview_question');
		var collection = db.get('userlist');

		collection.findOne({ 'username': 'test user' }, function (err, doc) {
			request(url)
				.put('/users/updateuser/' + doc._id)
				.send({})
				.expect(404)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(res));
						throw err;
					}
					expect(res.body).toBeTruthy();
					expect(res.body.username).toBeUndefined();
					assert.deepEqual(res.body, { msg: 'No data included in request. Please check your request body' });
					db.close();
					done();
			});
		});
	});
});