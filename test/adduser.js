var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');

var url = 'http://localhost:3000';

describe('Add User', function(){

	before(() => {
		const db = monk('localhost:27017/node_interview_question');
		db.collection('userlist').drop();
	});
	
	it('Adds a new user with user name \'test user\'', function(done) {	
		const newUser = {
			'username' : 'test user',
			'email' : 'test1@test.com',
			'fullname' : 'Bob Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};
	
		request(url)
		.post('/users/adduser')
		.send(newUser)
		.expect(201)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			expect(res.body._id).toBeDefined();
			delete res.body._id;
			assert.deepEqual(res.body, newUser);
			done();
		});
	});

	it('should return a 404 if there is no request body, with proper response', function(done) {
		request(url)
		.post('/users/adduser')
		.expect(404)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			expect(res.body).toBeDefined();
			assert.deepEqual(res.body, { msg: 'No data included in request. Please check your request body' });
			done();
		});
	});

	it('should return a 404 if there is incomplete data, with proper response', function(done) {
		const missingUserName = {
			'email' : 'test1@test.com',
			'fullname' : 'Bob Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};
	
		request(url)
		.post('/users/adduser')
		.send(missingUserName)
		.expect(404)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			expect(res.body).toBeDefined();
			assert.deepEqual(res.body, { msg: 'Incomplete data received. Please check your request body to make sure all fields are complete' });
			done();
		});
	});

	it('should return a 404 if there are other incomplete fields, with proper response', function(done) {
		const missingLocation = {
			'username' : 'test user',
			'email' : 'test1@test.com',
			'fullname' : 'Bob Smith',
			'age' : 27,
			'gender' : 'Male'
			};
	
		request(url)
		.post('/users/adduser')
		.send(missingLocation)
		.expect(404)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			expect(res.body).toBeDefined();
			assert.deepEqual(res.body, { msg: 'Incomplete data received. Please check your request body to make sure all fields are complete' });
			done();
		});
	})
});