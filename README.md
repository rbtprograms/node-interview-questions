## Robert Thompson SBI Code Challenge Submission

- Frontend built with React. I tried to keep the frontend style more or less what was provided to me, outside of some minor tweaks. Installed more robust error handling on the routes and made the tests pass consistently. Increased the test suite to account for more cases. 

### How to use

- run 'npm install'
- run 'npm test' to see all tests passing
- next we should start up the app to see it in action
- run 'npm run serve' to start server
- run 'npm start' to deploy local server, navigate to localhost:8080 (this should happen automatically)
- Begin interacting! 

### Reasoning

1. Always sending back 500 on delete error.
  - I find it to be better to not include robust error messages back to users about what is happening when deletes fail. My reasoning for this is if someone is maliciously attempting to send delete requests that are not working, they should not be getting back hints about what they are doing wrong. I simply send back a 500 and a message saying 'Error deleting document'. Developers should know that the problem is the lookup went bad and where to look. I think that the benefits of not kicking back a lot of different possibilities outweighs any of the problems it may create.
1. Before drop collection hook on tests.
  - I like to have my tests always deal with the same data. I like setting up either before or beforeEach hooks that make sure my tests always have the same information, just in case I have gone into the database and started tinkering with things and then run my tests, they always pass. To achieve this, I added a before hook that drops the userlist collection at the start of my tests. By dropping the collection, I am always starting with a fresh slate and can ensure that my tests are consistently passing. Of course, this will erase all the data from that collection, so a tests should never be deployed and run in a production environment. I find it extremely helpful for testing purposes though!
1. Frontend.
  - I tried to keep the frontend in as close a style as what was given to me as possible. Ultimately I viewed this more as a functional challenge rather than a design challenge, and I wanted something basic that would work well. Additionally, I chose nto to use Redux on this. It's a rather small app, and Redux seemed a bit heavy handed for what was going on in here. I have not used React without Redux for a while, so it was a fun challenge figuring out some of the state management problems without Redux! 